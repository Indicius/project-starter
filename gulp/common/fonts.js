const gulp = require('gulp')
const config = require('../config')

gulp.task('fonts', () =>
  gulp.src(config.project.fontFile)
    .pipe(gulp.dest(config.directories.public.fonts))
)
