const gulp = require('gulp')
const config = require('../config')
const browserify = require('browserify')
const babelify = require('babelify')
const sourcemaps = require('gulp-sourcemaps')
const source = require('vinyl-source-stream')
const rename = require('gulp-rename')
const buffer = require('vinyl-buffer')
const uglify = require('gulp-uglify')

gulp.task('scripts', () =>
  browserify({
    entries: [config.project.jsMain]
  })
    .transform(babelify, { presets: ['@babel/env'] })
    .bundle()
    .pipe(source(config.project.jsMain))
    .pipe(rename({
      dirname: '',
      basename: 'main',
      extname: config.production ? '.min.js' : '.js'
    }))
    .pipe(buffer())
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(uglify())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(config.directories.public.script))
)