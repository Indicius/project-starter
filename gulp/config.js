'use strict'
// Reads flags in the CLI
const flags = require('yargs').argv
// Project Conditions
const production = !!flags.prod || !!flags.production
const debug = !!flags.debug
const tunnel = !!flags.tunnel

module.exports = {
  directories: {
    src: {
      base: 'src',
      markup: 'src/markup',
      fonts: 'src/assets/fonts',
      icons: 'src/assets/icons',
      images: 'src/assets/images',
      scripts: 'src/assets/js',
      styles: 'src/assets/styles'
    },
    public: {
      base: 'build',
      markup: 'build',
      fonts: 'build/assets/fonts',
      icons: 'build/assets/icons',
      images: 'build/assets/images',
      script: 'build/assets/js',
      styles: 'build/assets/css'
    }
  },
  project: {
    cssFiles: 'src/assets/styles/main/**/!(_)*.scss',
    cssVendor: 'src/assets/styles/vendor/vendor.scss',
    jsMain: 'src/assets/js/index.js',
    fontFile: [
      'src/assets/fonts/**/*'
    ]
  },
  onError: function (error) {
    console.log(error.toString())
    production
      ? process.exit(1)
      : this.emit('end')
  },
  production,
  debug,
  tunnel,
  // Purify
  purify: ['./build/**/*.js', './build/**/*.html']
}